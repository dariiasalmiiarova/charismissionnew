import React from 'react';
import footerLogo from '../../Images/logo-english_white.svg'
import facebookLogo from '../../Images/facebook-logo.webp'
import facebookLike from '../../Images/like-3.gif'
import Button from '@material-ui/core/Button';

export default class Footer extends React.Component {
    render() {
        return(
            <footer className="footer-main">
                <div className="footer-contant container">
                    <div className="footer-row">
                        <img className="footer-logo" alt="CharisMission" src={footerLogo}></img>
                    </div>
                    <div className="footer-row contact-info-footer">
                        <h4 className="footer-title-column">Kontakt</h4>
                        <div className="info-footer-contact-row">
                            <div className="icon-main"><i class="fas fa-envelope"></i></div>
                            <div className="contact-info-main-footer">
                                <div className="contact-info-title">Email:</div>
                                <a href="mailto:info@charismission.org" className="contact-info-answer">info@charismission.org</a>
                            </div>
                        </div>
                        <div className="info-footer-contact-row">
                            <div className="icon-main"><i class="fas fa-phone"></i></div>
                            <div className="contact-info-main-footer">
                                <div className="contact-info-title">Telefon:</div>
                                <a href="tel:+48600562222" className="contact-info-answer">+48 600 562 222</a>
                            </div>
                        </div>
                        <div className="info-footer-contact-row">
                            <div className="icon-main"><i class="fas fa-envelope"></i></div>
                            <div className="contact-info-main-footer">
                                <div className="contact-info-title">Miejscowość:</div>
                                <p className="contact-info-answer">Bugeso, Uganda</p>
                            </div>
                        </div>
                    </div>
                    <div className="footer-row">
                        <h4 className="footer-title-column">Social Media</h4>
                        <a href="https://www.facebook.com/charismission"><img className="facebook-like" src={facebookLike} alt="CharisMission Facebook"></img></a>
                        <div className="button-container footer-btn-donate">
                            <Button onClick={this.props.openPopupDonate} variant="outlined" className="header-donate-button">Przekaż Darowiznę</Button>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}