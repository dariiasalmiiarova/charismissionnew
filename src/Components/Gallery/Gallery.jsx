import React from 'react';
import ImageGallery from 'react-image-gallery';
import LazyLoad from 'react-lazy-load';



export default class Gallery extends React.Component {
    constructor(){
        super();
        this.state = {
            photo:[]
        }
    }

    componentDidMount(){
        fetch('https://strapi-charis-backend.herokuapp.com/galleries')
        .then(responce => responce.json())
        .then(photo => this.setState({photo: photo[0].photo}))
    }

    
    render() {
        console.log(this.state.photo);
        var imgArr = [];
        var apiUrl = 'https://strapi-charis-backend.herokuapp.com'
        this.state.photo.map(item => {
            imgArr.push({
                original: apiUrl+item.formats.large.url,
                thumbnail: apiUrl+item.formats.small.url
            })
        })
        // console.log(imgArr)
        // const elements = this.state.photos.map(item => {
        //     return(
        //     <img src={`http://localhost:1337${item.formats.large.url}`}></img>
        //     )
        // })
        return (
            <div id="gallery" className="gallery-component-main">
                <div className="main-title-div">
                    <div className="main-title">
                        <h2 className="about-us-title h2-title">Galeria</h2>
                        <div className="title-line"></div>
                    </div>
                </div>
                <div className="container-gallery ">
                <LazyLoad debounce={false} offsetVertical={50}>
                <ImageGallery items={imgArr} />
                </LazyLoad>
                </div>
                
               
            </div>
        );
    }
}