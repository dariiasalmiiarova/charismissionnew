import React from 'react';
import { Formik } from 'formik';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import PolitykaPopup from './PolitykaPopup.component';

export default class ContactUsPopup extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            surname: '',
            email: '',
            phone: '',
            subject: '',
            message: '',
        }
    }
    onLabelChangeName = (e) => {
        this.setState({
            name: e.target.value
        })
    }
    onLabelChangeSurname = (e) => {
        this.setState({
            surname: e.target.value
        })
    }
    onLabelChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }
    onLabelChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        })
    }
    onLabelChangeSubject = (e) => {
        this.setState({
            subject: e.target.value
        })
    }
    onLabelChangeMessage = (e) => {
        this.setState({
            message: e.target.value
        })
    }
    render(){
        console.log(this.state)
        return(
            <div className="kontakt-main-popup" id="contact">
                <div className="form">
                    <Formik 
                        validate={values => {
                            let errors ={}
                            if(values.email){
                                errors.email = 'Required'
                            } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)){
                                { return errors }
                            }
                        }}
                    initialValues={{
                        name: '',
                        surname: '',
                        email: '',
                        phone: '',
                        theme: '',
                        message: ''
                    }}
                    onSubmit={(values, {setSubmitting}) => {
                    setTimeout(() => {
                        console.log(JSON.stringify(values, null, 2))
                        setSubmitting(false)
                    }, 10000)
                    console.log(values);
                    var AwsSesMail = require('aws-ses-mail')
                    var mail = new AwsSesMail()
                    var sesConfig = {
                        accessKeyId: 'AKIAIIPJ6QWH2N2KXZ6Q',
                        secretAccessKey: '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',
                        region: 'eu-central-1'
                    }
                    mail.setConfig(sesConfig)
                        var options ={
                            from: 'CharisMission Sender <sender@charismission.org>',
                            to:'<info@charismission.org>',
                            subject: this.state.subject,
                            content:'<html><head></head><body><div><p><br/><b>Imię:</b>' + this.state.name + '<br/><b>Nazwisko</b>:' + this.state.surname + '<br/><b>Email: </b>' + this.state.email + '<br/><b>Telefon:</b> </br>' + this.state.phone + '<br/><b>Wiadomość:</b> </br>' + this.state.message + '</p></div><div></br></br><b>Wyslane ze strony charismission.org</b></div></body></html>'
                        }
                        mail.sendEmail(options, function(data){
                            console.log(data);
                        })
                        this.setState({
                            name: '',
                            surname: '',
                            email: '',
                            phone: '',
                            subject: '',
                            message: ''
                        })
                        }}>
                {({
                    values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting
                }) =>(
                    <form className="form-main container-form-popup" onSubmit={handleSubmit}>
                        <div className="form-row-popup">
                            <div className="form-input-full form-input-block-popup">
                                <TextField 
                                    className="popup-form-input"
                                    id="outlined-basic" 
                                    label="Twoje Imię" 
                                    // variant="outlined" 
                                    name='name'
                                    // id='name'
                                    type='text'
                                    onChange={this.onLabelChangeName}
                                    value={this.state.name}
                                    variant='outlined'
                                    margin='normal'
                                    required 
                                    />
                            </div>
                        </div>
                        <div className="form-input-full form-input-block-popup">
                            <TextField id="outlined-basic" label="Email" 
                                name='email'
                                // id='email'
                                type='email'
                                onChange={this.onLabelChangeEmail}
                                value={this.state.email}
                                variant='outlined'
                                margin='normal'
                                required 
                                />
                        </div>
                        <div className="form-input-full form-input-block-popup">
                            <TextField id="outlined-basic" label="Telefon"  
                                name='phone'
                                // id='phone'
                                type='phone'
                                onChange={this.onLabelChangePhone}
                                value={this.state.phone}
                                variant='outlined'
                                margin='normal'
                                />
                        </div>
                        <div className="form-input-full form-input-block-popup">
                            <TextField
                                label="Wiadomość"
                                name='message'
                                onChange={this.onLabelChangeMessage}
                                value={this.state.message}
                                variant='outlined'
                                margin='normal'
                                multiline
                                rows={4}
                                required
                                />
                        </div>
                        <div className="checkbox-main checkbox-main-pop">
                            <input id="checkbox" type="checkbox"></input>
                            <label className="checkbox-text" htmlFor="checkbox">Wyrażam zgodę na przetwarzanie moich danych osobowych - </label>
                            <label className="checkbox-text" htmlFor="checkbox" onClick={this.props.onClockChP} className="policy-link" >polityka prywatności</label>
                            <PolitykaPopup onClose = {this.props.onCloseP} onClick = {this.props.onClickP}/>
                        </div>
                        <div className="contact-form-button">
                            <Button type="submit" variant="outlined" className="header-donate-button school-button contact-form-btn-pop" >Wyślij</Button>
                    </div>
                        
                    </form>
                )}
            </Formik>
       </div>
    </div>
        );
    }
}