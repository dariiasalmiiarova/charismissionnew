import React from 'react';
import Button from '@material-ui/core/Button';
import ContactUsPopup from './ContactUsPopup';

const PopupInfo = () => (
    <div className="popup-info-main">
        <div className="modal-row">
            <div className="modal-title">Formularz kontaktowy</div>
            <ContactUsPopup />
        </div>
        <div className="modal-row">
            <div className="modal-title">Konto bankowey</div>
            <div className="title-bank konto-line">Bank Pekao S.A.</div>
            <div className="konto-bankowe">28 1240 1503 1111 0010 8272 1371</div>
            <div className="tytul-do-konta">z dopiskiem "Darowizna na cel projektu (nazwa projektu)"</div>
            <p className="konto-p">Każdemu Darczyńcy serdecznie dziękujemy za przekazane środki. Cieszymy się, że realizowane przez nas projekty są Wam bliskie. Wspaniale jest wiedzieć, że możemy na Was liczyć!</p>
            <div className="popup-button">
                <Button type="submit" variant="outlined" className="header-donate-button school-button contact-form-btn-ch" >Naze Projekty</Button>
            </div>
        </div>
    </div>
)

export default PopupInfo;