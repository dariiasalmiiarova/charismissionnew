import React from 'react';

const Polityka = () => (
    <div className="polityka-main">
        <div className="paragrph">
            <p className="title-p">§.1 Postanowienia Ogólne</p>
            <ol className="p-item">1.Administratorem danych jest Kościół Chrystusowy Hosanna w Lublinie z siedzibą w Lublin, ul. Hutnicza 20C, 20-218 LUBLIN, NIP: 712-27-90-946, REGON: 432528229. Ochrona danych odbywa się zgodnie z wymogami powszechnie obowiązujących przepisów prawa, a ich przechowywanie ma miejsce na zabezpieczonych serwerach.</ol>
            <ol className="p-item">2.Dla interpretacji terminów stosuje się słowniczek Regulaminu lub tak jak zostało to opisane w Polityce Prywatności (jeżeli wynika to bezpośrednio z opisu).</ol>
            <ol className="p-item">3.Na potrzeby lepszego odbioru Polityki Prywatności termin „Użytkownik” zastąpiony został określeniem „Ty”, „Administrator” – „My”. Termin „RODO” oznacza Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE.</ol>
            <ol className="p-item">4.Szanujemy prawo do prywatności i dbamy o bezpieczeństwo danych. W tym celu używany jest m.in. bezpieczny protokół szyfrowania komunikacji (SSL).</ol>
            <ol className="p-item">5.Dane osobowe podawane w formularzu na stronie internetowej są traktowane jako poufne i nie są widoczne dla osób nieuprawnionych.</ol>
        </div>
        
        <div className="paragrph">
            <p className="title-p">§2. Administrator Danych</p>
            <ol className="p-item">1.Usługodawca jest administratorem danych swoich klientów. Oznacza to, że jeśli posiadasz Konto na naszej stronie, to przetwarzamy Twoje dane jak: imię, nazwisko, adres e-mail, numer telefonu, stanowisko, miejsce pracy, adres IP.</ol>
            <ol className="p-item">2.Usługodawca jest także administratorem osób zapisanych na newsletter.</ol>
            <ol className="p-item">3.Dane osobowe przetwarzane są:</ol>
            <ol className="p-item">a. zgodnie z przepisami dotyczącymi ochrony danych osobowych,</ol>
            <ol className="p-item">b. zgodnie z wdrożoną Polityką Prywatności,</ol>
            <ol className="p-item">c. w zakresie i celu niezbędnym do nawiązania, ukształtowania treści Umowy, zmiany bądź jej rozwiązania oraz prawidłowej realizacji Usług świadczonych drogą elektroniczną</ol>
            <ol className="p-item">d. w zakresie i celu niezbędnym do wypełnienia uzasadnionych interesów (prawnie usprawiedliwionych celów), a przetwarzanie nie narusza praw i wolności osoby, której dane dotyczą:</ol>
            <ul>w zakresie i celu zgodnym ze zgodą wyrażoną przez Ciebie jeśli [przykładowo] zapisałeś się na newsletter,</ul>
            <ul>w zakresie i celu zgodnym z wyrażoną przez Ciebie zgodą jeżeli [przykładowo] zapisałeś się na webinar.</ul>
            <ol className="p-item">4.Każda osoba, której dane dotyczą (jeżeli jesteśmy ich administratorem) ma prawo dostępu do danych, sprostowania, usunięcia lub ograniczenia przetwarzania, prawo sprzeciwu, prawo wniesienia skargi do organu nadzorczego.</ol>
            <ol className="p-item">5.Kontakt z osobą nadzorującą przetwarzanie danych osobowych w organizacji Usługodawcy jest możliwy drogą elektroniczną pod adresem e-mail: info@timdesign.top.</ol>
            <ol className="p-item">6.Zastrzegamy sobie prawo do przetwarzania Twoich danych po rozwiązaniu Umowy lub cofnięciu zgody tylko w zakresie na potrzeby dochodzenia ewentualnych roszczeń przed sądem lub jeżeli przepisy krajowe albo unijne bądź prawa międzynarodowego obligują nas do retencji danych.</ol>
            <ol className="p-item">7.Zastrzegamy sobie prawo do przetwarzania Twoich danych po rozwiązaniu Umowy lub cofnięciu zgody tylko w zakresie na potrzeby dochodzenia ewentualnych roszczeń przed sądem lub jeżeli przepisy krajowe albo unijne bądź prawa międzynarodowego obligują nas do retencji danych.</ol>
            <ol className="p-item">8.Usunięcie danych osobowych może nastąpić na skutek cofnięcia zgody bądź wniesienia prawnie dopuszczalnego sprzeciwu na przetwarzanie danych osobowych.</ol>
            <ol className="p-item">9.Usługodawca nie udostępniania danych osobowych innym podmiotom aniżeli upoważnionym na podstawie właściwych przepisów prawa.</ol>
            <ol className="p-item">10.Wdrożyliśmy pseudonimizację, szyfrowanie danych oraz mamy wprowadzoną kontrolę dostępu dzięki czemu minimalizujemy skutki ewentualnego naruszenia bezpieczeństwa danych.</ol>
            <ol className="p-item">11.Dane osobowe przetwarzają osoby wyłącznie upoważnione przez nas albo przetwarzający, z którymi ściśle współpracujemy.</ol>
        </div>
        <div className="paragrph">
            <p className="title-p">§3. Pliki cookies</p>
            <ol className="p-item">1. Witryna timdesign.top używa cookies. Są to niewielkie pliki tekstowe wysyłane przez serwer www i przechowywane przez oprogramowanie komputera przeglądarki. Kiedy przeglądarka ponownie połączy się ze stroną, witryna rozpoznaje rodzaj urządzenia, z którego łączy się użytkownik. Parametry pozwalają na odczytanie informacji w nich zawartych jedynie serwerowi, który je utworzył. Cookies ułatwiają więc korzystanie z wcześniej odwiedzonych witryn.<br/> Gromadzone informacje dotyczą adresu IP, typu wykorzystywanej przeglądarki, języka, rodzaju systemu operacyjnego, dostawcy usług internetowych, informacji o czasie i dacie, lokalizacji oraz informacji przesyłanych do witryny za pośrednictwem formularza kontaktowego.</ol>
            <ol className="p-item">2.Zebrane dane służą do monitorowania i sprawdzenia, w jaki sposób użytkownicy korzystają z naszych witryn, aby usprawniać funkcjonowanie serwisu zapewniając bardziej efektywną i bezproblemową nawigację. Monitorowania informacji o użytkownikach dokonujemy korzystając z narzędzia Google Analytics, które rejestruje zachowanie użytkownika na stronie.<br /> Cookies identyfikuje użytkownika, co pozwala na dopasowanie treści witryny, z której korzysta, do jego potrzeb. Zapamiętując jego preferencje, umożliwia odpowiednie dopasowanie skierowanych do niego reklam. Stosujemy pliki cookies, aby zagwarantować najwyższy standard wygody naszego serwisu, a zebrane dane są wykorzystywane jedynie wewnątrz firmy BOHDAN TYMOSHENKO TIMDESIGN w celu optymalizacji działań.</ol>
            <ol className="p-item">3.Na naszej witrynie wykorzystujemy następujące pliki cookies</ol>
            <ol className="p-item">a) „niezbędne” pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach serwisu, np. uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania w ramach serwisu;</ol>
            <ol className="p-item">b) pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania w ramach serwisu;</ol>
            <ol className="p-item">c) „wydajnościowe” pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych serwisu;</ol>
            <ol className="p-item">„funkcjonalne” pliki cookies, umożliwiające „zapamiętanie” wybranych przez użytkownika ustawień i personalizację interfejsu użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.;</ol>
            <ol className="p-item">e) „reklamowe” pliki cookies, umożliwiające dostarczanie użytkownikom treści reklamowych bardziej dostosowanych do ich zainteresowań.</ol>
            <ol className="p-item">4. Użytkownik w każdej chwili ma możliwość wyłączenia lub przywrócenia opcji gromadzenia cookies poprzez zmianę ustawień w przeglądarce internetowej.</ol>
            <p className="p-item last-p">Dodatkowe dane osobowe, jak adres e-mail, zbierane są jedynie w miejscach, w których użytkownik wypełniając formularz wyraźnie wyraził na to zgodę. Powyższe dane zachowujemy i wykorzystujemy tylko do potrzeb niezbędnych do wykonania danej funkcji.</p>
        </div>
    </div>
)

export default Polityka;