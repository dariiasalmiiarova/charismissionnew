import React from 'react';
import { Formik } from 'formik';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button'; 
import { colors } from '@material-ui/core';

/* Access Key ID: AKIAIIPJ6QWH2N2KXZ6Q
Secret Access Key: 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 */
export default class PopupDonateForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            phone: '',
            message: '',
        }
    }
    onLabelChangeName = (e) => {
        this.setState({
            name: e.target.value
        })
    }
    onLabelChangeSurname = (e) => {
        this.setState({
            surname: e.target.value
        })
    }
    onLabelChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }
    onLabelChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        })
    }
    onLabelChangeMessage = (e) => {
        this.setState({
            message: e.target.value
        })
    }
    render(){
        console.log(this.state)
        return(
<div className="popup-donate-main-form">
        <div className="form">
            <Formik 
                validate={values => {
                    let errors ={}
                    if(values.email){
                        errors.email = 'Required'
                    } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)){
                        { return errors }
                    }
                }}
            initialValues={{
                name: '',
                email: '',
                phone: '',
                message: ''
            }}
            onSubmit={(values, {setSubmitting}) => {
               setTimeout(() => {
                   console.log(JSON.stringify(values, null, 2))
                   setSubmitting(false)
               }, 10000)
               console.log(values);
               var AwsSesMail = require('aws-ses-mail')
               var mail = new AwsSesMail()
               var sesConfig = {
                   accessKeyId: 'AKIAIIPJ6QWH2N2KXZ6Q',
                   secretAccessKey: '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',
                   region: 'eu-central-1'
               }
               mail.setConfig(sesConfig)
               var options ={
                   from: 'CharisMission Sender <sender@charismission.org>',
                   to:'<info@charismission.org>',
                   subject: 'Formularz kontaktowy Popup',
                   content:'<html><head></head><body><div><p><br/><b>Imię:</b>' + this.state.name + '<br/><b>Email: </b>' + this.state.email + '<br/><b>Telefon:</b> </br>' + this.state.phone + '<br/><b>Wiadomość:</b> </br>' + this.state.message + '</p></div><div></br></br><b>Wyslane ze strony charismission.org "Popup"</b></div></body></html>'
               }
               mail.sendEmail(options, function(data){
                   console.log(data);
               })
               this.props.onModalO();
               this.setState({
                name: '',
                email: '',
                phone: '',
                message: ''
               })
            }}>
                {({
                    values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting
                }) =>(
                    <form className="form-main container-form-popup" onSubmit={handleSubmit}>
                        <div className="form-input-full">
                            <TextField id="outlined-basic" label="Twoje Imię" 
                                    name='name'
                                    // id='theme'
                                    type='text'
                                    onChange={this.onLabelChangeName}
                                    value={this.state.name}
                                    variant='outlined'
                                    margin='normal'
                                    required 
                                    />
                        </div>
                        <div className="form-input-full">
                            <TextField id="outlined-basic" label="Telefon" 
                                    name='phone'
                                    // id='theme'
                                    type='phone'
                                    onChange={this.onLabelChangePhone}
                                    value={this.state.phone}
                                    variant='outlined'
                                    margin='normal'
                                    />
                        </div>
                        <div className="form-input-full">
                            <TextField id="outlined-basic" label="Email" 
                                    name='email'
                                    // id='theme'
                                    type='email'
                                    onChange={this.onLabelChangeEmail}
                                    value={this.state.email}
                                    variant='outlined'
                                    margin='normal'
                                    required 
                                    />
                        </div>
                        <div className="form-input-full">
                            <TextField
                                // id="message"
                                label="Wiadomość"
                                name='message'
                                // defaultValue="Default Value"
                                
                                onChange={this.onLabelChangeMessage}
                                value={this.state.message}
                                variant='outlined'
                                margin='normal'
                                multiline
                                rows={6}
                                required
                                />
                        </div>
                        <div className="checkbox-main-popup">
                            <input required id="checkbox-popup" type="checkbox"></input>
                            <label className="checkbox-text" htmlFor="checkbox-popup">Wyrażam zgodę na przetwarzanie moich danych osobowych - <span className="policy-link" onClick={this.props.openPolicy}>polityka prywatności</span></label>
                        </div>
                        <div className="contact-form-button-popup">
                            <Button type="submit" variant="outlined" className="header-donate-button school-button contact-form-btn-ch">Wyślij</Button>
                    </div>
                        
                    </form>
                )}
            </Formik>
       </div>
    </div>
        );
    }
}
