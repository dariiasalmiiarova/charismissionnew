import React from 'react';
import PopupDonateForm from './PopupDonateForm'
import Button from '@material-ui/core/Button'; 


const PopupDonateInfo = (props) => {
    return(
        <div className="popup-donate-info">
            <div className="popup-donate-row">
                <div className="popup-donate-title">Formularz kontaktowy</div>
                <PopupDonateForm openPolicy={props.propsPopup} onModalO={props.onModalO} onCloseModal={props.onCloseModal}/>
            </div>
            <div className="popup-donate-row">
            <div className="popup-donate-title">Konto bankowe</div>
                <div className="popup-bank">Bank Pekao S.A.</div>
                <div className="popup-numer-rachunku">28 1240 1503 1111 0010 8272 1371</div>
                <div className="popup-title-przelewu">z dopiskiem "Darowizna na cel projektu (nazwa projektu)"</div>
                <div className="popup-description">Każdemu Darczyńcy serdecznie dziękujemy za przekazane środki. Cieszymy się, że realizowane przez nas projekty są Wam bliskie. Wspaniale jest wiedzieć, że możemy na Was liczyć!"</div>
                <div className="projects-cta-button">
                            <Button variant="outlined" className="header-donate-button school-button">Nasze projekty</Button>
                </div>
            </div>
        </div>
    )
}

export default PopupDonateInfo;