import React from 'react'

import { LazyLoadImage}from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import Button from '@material-ui/core/Button'; 
import { makeStyles } from '@material-ui/core/styles';
import TrackVisibility from 'react-on-screen';
import  CountUp  from 'react-countup';
import { CircularProgressbar, CircularProgressbarWithChildren, buildStyles } from "react-circular-progressbar";
import AnimatedProgressProvider from './AnimatedProgressProvider';
import { easeQuadInOut } from 'd3-ease';
import 'react-circular-progressbar/dist/styles.css';


import item1 from '../../Images/Projects/new.jpg';
import item2 from '../../Images/Projects/20170826_140517.jpg';
import item3 from '../../Images/Projects/20170824_163456.jpg';
import item4 from '../../Images/Projects/20170816_123104.jpg';
import item5 from '../../Images/Projects/DSC04409.JPG';
import item6 from '../../Images/Projects/20170826_155007.jpg';

class Projects extends React.Component {
    
    constructor(){
        super();
        this.state = {
            projectsData: [
                {id:1, imgUrl: item1, title: "Barbiriada", cel:6000, zebrano:360, info:"To akcja skupiająca osoby chcące odmienić los pewnej ugandyjskiej dziewczyny o imieniu Barbirye, która zmaga się z niepełnosprawnością fizyczną i ubóstwem, ale na pewno inspiruje wszystkich swoim urzekającym uśmiechem, skromnością i wytrwałością w cierpieniu. Dzięki szczodrej pomocy Darczyńców Barbirye chodzi dziś do szkoły i żyje nadzieją, że także ona ma szansę na lepsze życie.", celTitle:"Cel roczny: " },
                {id:2, imgUrl: item2, title: "Wdowia spółdzielnia", cel:20000, zebrano:750, info:"Inicjatywa pomocy wdowom z Bugeso, które pragną dźwignąć się z najniższego szczebla społeczeństwa i godnie zarabiać na swoje utrzymanie. Nie brakuje im pomysłów, ani woli walki o swój byt, ale brakuje im zasobów i ukierunkowania w kwestiach przedsiębiorczości. Możesz się zaangażować ofiarując dar finansowy na zakup maszyn do szycia oraz materiałów krawieckich.", celTitle:"Cel: " },
                {id:3, imgUrl: item3, title: "Wioska garncarska", cel:25000, zebrano:580, info:"Wesprzyj realizację projektu rzemieślniczo-edukacyjnego w Bugeso zakładającego założenie centrum garncarskiego, które byłoby atrakcją turystyczną dla zagranicznych turystów, przedsięwzięciem wspomagającym utrzymanie szkoły oraz samych garncarzy oraz centrum edukacyjnym, gdzie młodzi ugandyjczycy będą uczyć się zawodu.", celTitle:"Cel: " },
                {id:4, imgUrl: item4, title: "Szanse dla młodych - Medson, Sydney", cel:7500, zebrano:2100, info:"Pomagamy młodemu kierowcy boda-boda (motocyklowej taksówki) uniknąć bardzo niebezpiecznej pracy na ugandyjskich drogach poprzez umożliwienie kształcenia się na kierunku grafika komputerowego. Zostań jego przyjacielem, rodziną, której nie ma i zainwestuj w wykształcenie Medsona.", celTitle:"Cel roczny: " },
                {id:5, imgUrl: item5, title: "Posiłek dla dzieci w szkole", cel:30000, zebrano:0, info:"Kup posiłek dla dziecka, które przychodzi 5 razy w tygodniu na zajęcia w naszej polowej szkole w Bugeso. Każde 5zł pozwoli na kupno jednego posiłku dla jednego z 30 dzieci. Dla większości z nich jest to jedyny posiłek w ciągu dnia. Wesprzyj dzieci z Afryki.", celTitle:"Cel roczny: " },
                {id:6, imgUrl: item6, title: "Wspieranie kościoła", cel:42000, zebrano:350, info:"Na terenie naszej misji od czerwca 2018 roku działa Społeczność Chrześcijańska Hosanna. Niedzielne spotkania gromadzą zwykle kilkadziesiąt, głównie młodych osób, które modlą się, śpiewają, studiują Boże Słowo i spędzają ze sobą czas przy posiłku. Wspólnota potrzebuje regularnego wsparcia na zakup żywności, a także na inwestycje, takie jak zakup instrumentów i ubrań dla chóru oraz pokrycie różnych drobnych wydatków.", celTitle:"Cel roczny: " }
            ],
            projectsApi:[],
            showModal: false,
            showPopup: false
        }
      }

    componentDidMount() {
        fetch('https://strapi-charis-backend.herokuapp.com/projects')
        .then(responce => responce.json())
        .then(projectsApi => this.setState({projectsApi: projectsApi}))
    }

    onClickShowModal = () => {
        this.setState({showPopup: true})
    }
    handleCloseModal = () => {
        this.setState({
            showPopup: false
        })
    }


    render(){
        var projArr = [];
        var apiUrl = 'https://strapi-charis-backend.herokuapp.com'
        this.state.projectsApi.map(item => {
            projArr.push({
                cel: item.Cel,
                zebrano: item.Zebrano
            })
        })



        
        console.log(this.state.projectsApi)
        const elements = this.state.projectsApi.map(item => (
            <div className="alone-project" key={item.id}> 
                <div className="projects-img-div">
                    
                    <LazyLoadImage className="projects-img" alt={item.name} src={apiUrl + item.image[0].formats.medium.url} delayMethod="throttle" effect="blur"/>
                </div>
                <div className="projects-info-block">
                    <div className="circle-progress-animation">
                        <TrackVisibility>
                                {({ isVisible }) => isVisible && 
                                    <AnimatedProgressProvider
                                        valueStart={0}
                                        valueEnd={(100 * item.zebrane) / item.cel}
                                        duration={0.2}
                                        easingFunction={easeQuadInOut}
                                        >
                                        {(value) => {
                                            const roundedValue = Math.round(value);
                                            return (
                                                <CircularProgressbar
                                                    background={true}
                                                    value={value}
                                                    text={`${roundedValue}%`}
                                                    styles={buildStyles({pathTransitionDuration: 1.5, pathTransition: 'none' }), {
                                                        path: {
                                                            stroke: `rgba(239, 90, 27, 1)`,
                                                        },
                                                        text: {
                                                            fill: '#EF5A1B',
                                                            fontSize: '23px',
                                                        },
                                                        background: {
                                                            fill: '#ffffff',
                                                        },
                                                        trail: {
                                                            stroke: '#eee',
                                                        },
                                                    }}
                                                />
                                                );
                                            }}
                                    </AnimatedProgressProvider>
                                }
                        </ TrackVisibility>
                    </div>
                    <h3 className="projects-title">{item.name}</h3>
                    <div className="projects-cel">Cel:  
                        <TrackVisibility >
                            {({ isVisible }) => isVisible && 
                            <CountUp separator="" suffix=" PLN" start={0} end={item.cel}/>}
                        </TrackVisibility>
                    </div>
                    <div className="projects-zebrano"> Zebrano:  
                    <TrackVisibility >
                            {({ isVisible }) => isVisible && 
                            <CountUp separator="" suffix=" PLN" start={0} end={item.zebrane}/>}
                    </TrackVisibility>
                    </div>
                    <p className="projects-info">{item.info}</p>
                    <div className="projects-cta-button">
                            <Button onClick={this.props.openPopupDonate} type="submit" variant="outlined" className="header-donate-button school-button">Przekaż darowiznę</Button>
                    </div>
                </div>
                
                
            </div>
            
        ))
        console.log(this.state.showPopup)
        return(
            <div className="projects-main container">
                <div className="projects-row">
                    {elements}
                </div>
            </div>
        )
    }
}

export default Projects;