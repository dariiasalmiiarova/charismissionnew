import React from 'react';
import uganda from '../../Images/Image 3@2x.png'
import { LazyLoadImage}from 'react-lazy-load-image-component';

const Uganda = () => {
    return(
        <div className="uganda-main">
            <div className="main-title-div">
                <div className="main-title">
                    <h2 className="about-us-title h2-title">Uganda</h2>
                    <div className="title-line"></div>
                </div>
            </div>
            <div className="uganda-container container">

                <LazyLoadImage className="projects-img" alt="Uganda map" src={uganda} delayMethod="throttle" effect="blur"/>

                <div className="uganda-info">
                    <p className="uganda-info-p"><strong>Uganda</strong> – państwo w środkowej Afryce nad Jeziorem Wiktorii. Graniczy z Sudanem Południowym, Demokratyczną Republiką Konga, Rwandą, Tanzanią i Kenią. Ludność zajmuje się głównie rolnictwem i hodowlą, kraj słabo rozwinięty. Obecnie kraj znajduje się na liście 50 najbiedniejszych krajów świata.</p>
                    <br/>
                    <p className="uganda-info-ol-title"><strong>Kilka ciekawostek dotyczących Ugandy:</strong></p>
                    <ol className="uganda-info-ol">
                        <li>Stolicą jest Kampala</li>
                        <li>Gęstość zaludnienia to 136 osób/km²</li>
                        <li>Uganda jest 1,3 razy mniejsza od Polski</li>
                        <li>Ustrój polityczny – Republika Ugandy</li>
                        <li>Motto kraju: „Dla Boga i mojego kraju”</li>
                        <li>Religia – 84% chrześcijanie (katolicy + protestanci)</li>
                        <li>Waluta – szyling ugandyjski</li>
                        <li>Najwyższy szczyt – góra Stanleya 5 109 m n.p.m</li>
                        <li>Kraj uzyskał niepodległość od Wielkiej Brytanii w 1962r.</li>
                        <li>W Ugandzie mieszka ok. 100 Polaków</li>
                    </ol>
                </div>
            </div>
        </div>
    );
}

export default Uganda;