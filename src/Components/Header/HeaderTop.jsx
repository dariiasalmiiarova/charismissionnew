import React from 'react';
import logo from '../../Images/charis-logo.svg'


const HeaderTop = () => {
    return(
        <div id="home" className="container header-top">
            <div className="charis-logo-header-box">
                <img alt="CharisMission" className="header-logo" src={logo}></img>
            </div>
            <div className="charis-header-info">
                <div className="header-item">
                    <div className="header-item-img fas fa-envelope">
                    </div>
                    <div className="header-item-info">
                        <a className="header-links" href="mailto:info@charismission.org"><p>info@charismission.org</p></a>
                    </div>
                </div>
                <div className="header-item">
                    <div className="header-item-img">
                        <i className="fas fa-phone"></i>
                    </div>
                    <div className="header-item-info">
                        <a className="header-links" href="tel:+48574720320"><p>+48 600 562 222</p></a>
                    </div>
                </div>
                <div className="header-item">
                    <div className="header-item-img">
                        <i className="fas fa-map-marker-alt"></i>
                    </div>
                    <div className="header-item-info">
                        <p>Bugeso, Uganda</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HeaderTop;