import React from 'react';
import Button from '@material-ui/core/Button';
import poland from '../../Images/poland.svg'
import england from '../../Images/uk.svg'
import { HashLink } from 'react-router-hash-link';
import { BrowserRouter as Router } from "react-router-dom";


const HeaderMenu = (props) => {
    return(
        <Router>
        <div className="main-header-menu">
            <div className="container header-menu-component">
                <ul className="header-menu-ul">
                    <HashLink smooth  className="header-a-li-menu" to="#home"><li className="header-li-menu">Strona główna</li></HashLink>
                    <HashLink  smooth className="header-a-li-menu" to="#about-us"><li className="header-li-menu">O nas</li></HashLink>
                    <HashLink smooth className="header-a-li-menu" to="#projects"><li className="header-li-menu">Nasze projekty</li></HashLink>
                    <HashLink smooth className="header-a-li-menu" to="#news"><li className="header-li-menu">Aktualności</li></HashLink>
                    <HashLink smooth className="header-a-li-menu" to="#gallery"><li className="header-li-menu">Galeria</li></HashLink>
                    <HashLink smooth className="header-a-li-menu" to="#contact"><li className="header-li-menu">Kontakt</li></HashLink>
                </ul>
                <div className="flags-container">
                    <div className="button-container">
                        <Button onClick={props.openPopupDonate} variant="outlined" className="header-donate-button">Przekaż Darowiznę</Button>
                    </div>
                
                <div className="flags-box">
                    <img alt="Poland flag" className="flag" src={poland}></img>
                    <img alt="England flag" className="flag" src={england}></img>
                </div>
                </div>
                
            </div>
        </div>
        </Router>
    )
}

export default HeaderMenu;