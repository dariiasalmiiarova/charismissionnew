import React from 'react';
import mobileLogo from '../../Images/charis-logo.svg'
import poland from '../../Images/poland.svg'
import england from '../../Images/uk.svg'
import HamburgerMenu from 'react-hamburger-menu'
import Button from '@material-ui/core/Button';


export default class MobileHeader extends React.Component {
    constructor () {
        super();
        this.state = {
            open: false
        }
    }
    handleClick() {
        this.setState({
            open: !this.state.open
        });
    }
    render() {
        let mobileMenuClassNames = "mobile-menu-links-container";
        let mainMobileMenu = "main-mobile-menu";
        if(this.state.open) {
            mobileMenuClassNames += " active-mobile-menu";
            mainMobileMenu += " custom-mob-menu-shadow"
        }
        console.log(this.state);
        return (
            <div className="direct-menu">
                <div className={mainMobileMenu}>
                <div className="mobile-menu-container">
                <img alt="CharisMission" src={mobileLogo} className="mobile-header-logo"></img>
                <div className="right-block">
                    <div className="mobile-flags-box">
                        <img src={poland} alt="Poland flag" className="flags-mobile"/>
                        <img src={england} alt="English flag" className="flags-mobile"/>
                    </div>
                    <HamburgerMenu
                        className="hamburger-menu"
                        isOpen={this.state.open}
                        menuClicked={this.handleClick.bind(this)}
                        width={18}
                        height={15}
                        strokeWidth={1}
                        rotate={0}
                        color='black'
                        borderRadius={0}
                        animationDuration={0}
                    />
                    </div>
                    </div>
                </div>
                <div className={mobileMenuClassNames}>
                    <ul className="mobile-menu-ul">
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">Strona Główna</a></li>
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">O nas</a></li>
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">Nasze projekty</a></li>
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">Aktualności</a></li>
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">Galeria</a></li>
                        <li className="mobile-menu-item-li"><a href="#home" className="mobile-menu-link-a">Kontakt</a></li>
                        <Button variant="outlined" className="header-donate-button mobile-button-donate">Przekaż Darowiznę</Button>
                    </ul>
                </div>
            </div>
            
        );
    }
}