import React from 'react';
import Button from '@material-ui/core/Button'; 

const Slogan = (props) => {
    return(
        <div className="slogan-main">
            <div className="slogan-content">
                <p className="slogan-text">Łaską jesteście zbawieni <br/><span className="slogan-text-span">Efezjan 2:8</span></p>
                <div className="button-container">
                    <Button onClick={props.openPopupDonate} variant="outlined" className="header-donate-button">Przekaż Darowiznę</Button>
                </div>
            </div>
        </div>
    )
}

export default Slogan;