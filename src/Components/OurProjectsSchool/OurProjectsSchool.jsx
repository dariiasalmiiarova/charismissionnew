import React from 'react';
import SchoolSlider from './SchoolSlider';
import Button from '@material-ui/core/Button'; 
import ProgressSchoolBar from './ProgressSchoolBar';


const OurProjectsSchool = (props) => {
    return (
        <div id="projects" className="projects-school-main">
            <div className="main-title-div">
                <div className="main-title">
                    <h2 className="about-us-title h2-title">Nasze projekty</h2>
                    <div className="title-line"></div>
                </div>
            </div>
            <div className="school-subtitle">Budowa szkoły w Bugesso</div>
            <SchoolSlider />
            <p className="container school-slider-info"><strong>Budowa szkoły w Bugeso</strong> - zbieramy fundusze na budowę szkoły podstawowej w Bugeso w Ugandzie, która umożliwi dostęp do edukacji ponad 300 dzieciom z okolicy, które teraz są pozbawione możliwości kształcenia ze względu na ubóstwo. W kraju, w którym ok. 400 tysięcy dzieci nie chodzi do szkoły stworzenie warunków do uzyskania wykształcenia przynajmniej na poziomie podstawowym stanowi realny krok w kierunku wyjścia z ubóstwa. Jeżeli jest Ci bliska idea pomocy przez edukację, zachęcamy Cię do zaangażowania się finansowego. Twoje 30zł to X m2 szkoły, która może dać wykształcenie ponad 300 dzieciom.</p>
            <div className="button-container slider-cta-button container"><Button onClick={props.openPopupDonate} variant="outlined" className="header-donate-button school-button">Przekaż Darowiznę</Button></div>
            <ProgressSchoolBar />
        </div>
    );
}

export default OurProjectsSchool;