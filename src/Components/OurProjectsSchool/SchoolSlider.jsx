import React, { Component } from 'react';
import Slider from 'react-slick';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import img1school from '../../Images/SchoolSlider/IMG_5991.JPG'
import img2school from '../../Images/SchoolSlider/IMG_5989.JPG'
import img3school from '../../Images/SchoolSlider/IMG_5990.JPG'
import img4school from '../../Images/SchoolSlider/IMG_5992.JPG'
import img5school from '../../Images/SchoolSlider/IMG_5993.JPG'


export default class SchoolSlider extends Component {
    constructor(props) {
      super(props);
      this.state = {
        nav1: null,
        nav2: null
      };
    }
  
    componentDidMount() {
      this.setState({
        nav1: this.slider1,
        nav2: this.slider2
      });
    }
  
    render() {
      return (
        <div className="container">
          <Slider
            asNavFor={this.state.nav2}
            ref={slider => (this.slider1 = slider)}
          >
            <div className='school-slider-image-first'>
                <LazyLoadImage 
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img1school}
                    delayMethod="throttle"
                    />
            </div>
            <div className='school-slider-image-first'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img2school}
                    delayMethod="throttle"
                    />
            </div>
            <div className='school-slider-image-first'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img3school} 
                    delayMethod="throttle"                   
                    />
            </div>
            <div className='school-slider-image-first'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img4school}
                    delayMethod="throttle"
                    />
            </div>
            <div className='school-slider-image-first'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img5school}
                    delayMethod="throttle"
                    />
            </div>
          </Slider>
          <Slider
            asNavFor={this.state.nav1}
            ref={slider => (this.slider2 = slider)}
            slidesToShow={4}
            swipeToSlide={true}
            focusOnSelect={true}
          >
            <div className='school-slider-image-second'> 
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img1school}
                    delayMethod="throttle"
                   />
            </div>
            <div className='school-slider-image-second'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img2school}
                    delayMethod="throttle"
                    />
            </div>
            <div className='school-slider-image-second'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img3school}
                    delayMethod="throttle"
                   />
            </div>
            <div className='school-slider-image-second'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img4school}
                    delayMethod="throttle"
                 />
            </div>
            <div className='school-slider-image-second'>
            <LazyLoadImage
                    alt="Budowa szkoły CharisMission"
                    effect="blur"
                    src={img5school}
                    delayMethod="throttle"
                   />
            </div>
          </Slider>
        </div>
      );
    }
  }
