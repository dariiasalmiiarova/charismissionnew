import React from 'react';
import ProgressBar from './progressbar';
import TrackVisibility from 'react-on-screen';
import  CountUp  from 'react-countup';

export default class ProgressSchoolBar extends React.Component {
    constructor() {
        super();
        this.state = {
            schoolApi: []
        }
    }

    componentDidMount() {
        fetch('https://strapi-charis-backend.herokuapp.com/schools')
        .then(responce => responce.json())
        .then(schoolApi => this.setState({schoolApi: schoolApi[0]}))
    }

    
    render() {
        // const zebrane = this.state.schoolApi.Zebrano;
        // const potrzebne = this.state.schoolApi.Cel;
const percentage = this.state.schoolApi.zebrane * 100 / this.state.schoolApi.cel;
console.log(percentage);
        return(
            
                <div className="progress-school-main container">
            <div className="progress-zebrane">
                <div className="progress-title">Zebrane pieniądze:</div>
                <TrackVisibility partVisibility className="progress-lave">
                    {({ isVisible }) => isVisible ? <CountUp separator=" " suffix=" PLN" start={0} end={this.state.schoolApi.zebrane}/> : <p>0 PLN</p> }
                </TrackVisibility>
                
            </div>
            <TrackVisibility partVisibility className="progress-line">
                {({ isVisible }) => isVisible ? <ProgressBar percentage ={percentage}/> : <p style={{textAlign: "center"}}>Loading...</p>}
            </TrackVisibility>
            <div className="progress-potrzebne">
                <div className="progress-title">Potrzebujemy pieniądze:</div>
                <TrackVisibility partVisibility className="progress-lave">
                    {({ isVisible }) => isVisible ? <CountUp separator=" " suffix=" PLN" start={0} end={this.state.schoolApi.cel}/> : <p>0 PLN</p> }
                </TrackVisibility>
            </div>
        </div>
        )
    }
}
