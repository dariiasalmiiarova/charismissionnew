import React from 'react';

import HeaderTop from '../Components/Header/HeaderTop';
import HeaderMenu from '../Components/Header/HeaderMenu';
import { StickyContainer, Sticky } from 'react-sticky';
import MobileMenu from '../Components/Header/MobileHeader';
import Slogan from '../Components/Slogan/Slogan'
import AboutUs from '../Components/AboutUs/AboutUs'
import SeparatorImg from '../Components/SeparatorImg'
import Uganda from '../Components/Uganda/Uganda'
import OurProjectsSchool from '../Components/OurProjectsSchool/OurProjectsSchool';
import Projects from '../Components/Projects/Projects.component';
import FacebookPosts from '../Components/Facebook/FacebookPosts'
import Gallery from '../Components/Gallery/Gallery'
import ContactUs from '../Components/ContactUs/ContactUs'
import Footer from '../Components/Footer/Footer'
import PopupDonate from '../Components/PopupDonate/PopupDonate'
import PrivacyPopup from '../Components/PrivacyPopup/PrivacyPopup'
import SendMessage from '../Components/Success/SendMessage.component';


export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            popupDonateOpen: false,
            policyPopupOpen: false,
            sendMessageOpen: false
            
        }
    }

    openPopupDonate = () => {
        this.setState({
            popupDonateOpen: true
        })
    }
    closePopupDonate = () => {
        this.setState({
            popupDonateOpen: false
        })
    }
    openPopupPolicy = () => {
        this.setState({
            policyPopupOpen: true
        })
    }
    closePopupPolicy = () => {
        this.setState({
            policyPopupOpen: false
        })
    }
    openModalSend = () => {
        this.setState({
            sendMessageOpen: true
        })
    }
    closeModalClose = () => {
        this.setState({
            sendMessageOpen: false
        })
    }
    closePopupStatus = () => {
        if (this.state.sendMessageOpen === true){
            setTimeout(() => {
                this.setState({
                    sendMessageOpen: false
                })
            }, 3000)
        }
    }

    render() {
        console.log("Open: " + this.state.popupDonateOpen)
        // setTimeout(this.props.onCloseModal(), 1000);
        return(
            <div>
            <div className="header-top-test">
                <HeaderTop />
            </div>
            <div className="sticky-mobile">
                <MobileMenu />
            </div>
            
                <StickyContainer>
                    <div className="sticky-desktop">
                        <Sticky >{({ style }) => <div className="custom-padding-mobile-menu" style={style}><HeaderMenu openPopupDonate={this.openPopupDonate}/></div>}</Sticky>
                    </div>
                    {/* <div className="sticky-mobile">
                        <Sticky>{({ style }) => <div className="custom-padding-mobile-menu" style={style}><MobileMenu /></div>}</Sticky>
                    </div> */}
                    
                    <Slogan openPopupDonate={this.openPopupDonate}/>
                    <AboutUs/>
                    <SeparatorImg />
                    <Uganda />
                    <SeparatorImg />
                    <OurProjectsSchool openPopupDonate={this.openPopupDonate}/>
                    <SeparatorImg />
                    <Projects openPopupDonate={this.openPopupDonate}/>
                    <SeparatorImg />
                    <FacebookPosts />
                    <SeparatorImg />
                    <Gallery />
                    <SeparatorImg />
                    <ContactUs openPopupPolicy={this.openPopupPolicy} onModalO={this.openModalSend} onCloseModal={this.closeModalClose}/>
                    <Footer openPopupDonate={this.openPopupDonate}/>
                    <PopupDonate openPopupPolicy={this.openPopupPolicy} closePopupDonate={this.closePopupDonate} statePopup={this.state.popupDonateOpen} onModalO={this.openModalSend} onCloseModal={this.closeModalClose} />
                    <PrivacyPopup closePopupPolicy={this.closePopupPolicy} statePrivacy={this.state.policyPopupOpen} />
                    <SendMessage modalState={this.state.sendMessageOpen} onCloseModal={this.closePopupStatus}/>
                    
                </StickyContainer>
            
        </div>
        )
    }
}
