import React from 'react';
import { FacebookProvider, EmbeddedPost, Page } from 'react-facebook';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";





export default class FacebookPosts extends React.Component {


    render(){

        return(
            <div id="news" className="container facebook-posts-main">
        <div className="main-title-div">
                <div className="main-title">
                    <h2 className="about-us-title h2-title">Aktualności</h2>
                    <div className="title-line"></div>
                </div>
            </div>
            <div className="facebook-page-component">
            <FacebookProvider appId="578188749753246">
                <Page href="https://www.facebook.com/charismission" width="800" tabs="timeline" />
            </FacebookProvider>
            </div>
          
      </div>
        )
    }
}



