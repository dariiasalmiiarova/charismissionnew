import React from 'react';
import Button from '@material-ui/core/Button'; 
import aboutImg from '../../Images/about-us-img.jpg'
import { LazyLoadImage}from 'react-lazy-load-image-component';


const AboutUs = () => {
    return(
        <div id="about-us" className="about-us-main container">
            <div className="main-title-div">
                <div className="main-title">
                    <h2 className="about-us-title h2-title">O nas</h2>
                    <div className="title-line"></div>
                </div>
            </div>
            <div className="about-us-info">
                <div className="about-us-info-text">
                    <p className="main-text">Jesteśmy małżeństwem z Lublina, ale Afryka siedziała gdzieś w nas uśpiona. Aż do 2017 roku kiedy wyciągnęliśmy nasze pragnienia z szuflady i pojechaliśmy do Ugandy. Chociaż kraj urzekał krajobrazami, przyrodą, barwami i odgłosami, ale nasze serce zdobyli ludzie i ich wyzwania.</p><br/>
                    <p className="main-text custom-margin-about-us">Do Polski wróciliśmy z ich historiami i czuliśmy się odpowiedzialni za to, żeby opowiadać je innym z nadzieją, że może ich też poruszą i skłonią do zaangażowania w ich życie. Stąd Charis Mission, czyli misja, którą poczuła dwójka Charisów, aby zrobić co w naszej mocy, żeby zarazić innych chęcią pomocy wspaniałym mieszkańcom Ugandy.</p>
                        <div className="button-container">
                            <Button variant="outlined" className="header-donate-button about-button">Kontakt z nami</Button>
                        </div>
                </div>
                <div className="about-us-info-img">
                <LazyLoadImage className="projects-img" alt="CharisMission" src={aboutImg} delayMethod="throttle" effect="blur"/>

                </div>
            </div>
            
        </div>
    );
}
export default AboutUs;